import './bootstrap';

import VueRouter from 'vue-router';
import {ClientTable} from 'vue-tables-2';
import Notifications from 'vue-notification';

import App from './App';
import Home from './components/Home';
import Temperature from './components/Temperature';
import Orders from './components/Orders';
import OrdersTable from './components/OrdersTable';
import OrdersEdit from './components/OrderEdit';

import Vue from 'vue';

const routes = [
    {path: '/', name: 'home', component: Home},
    {path: '/temperature', name: 'temperature', component: Temperature},
    {path: '/orders',
        name: 'orders',
        component: Orders,
        children: [
            { path: 'all', name: 'orders.all', component: OrdersTable },
            { path: 'edit/:id', name: 'orders.edit', component: OrdersEdit, props: true }
        ]
    },
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(Notifications);

const app = new Vue({
    el: '#app',
    components: {App},
    router
});
