<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function(){

    Route::get('/weather/{city}', 'WeatherController@current');
    Route::get('/orders', 'OrderController@getAll');
    Route::get('/orders/{id}', 'OrderController@getById');
    Route::post('/orders/{id}', 'OrderController@edit');
    Route::get('/partners', 'PartnerController@all');

});

Route::get('/{any}', 'SiteController@index')->where('any', '.*');
