<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 6/23/19
 * Time: 4:40 AM
 */

namespace App\Services;


use App\Partner;

class PartnerService
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        /** @var Partner $partners */
        $partners = Partner::all();
        return $partners;
    }
}
