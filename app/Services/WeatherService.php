<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 6/21/19
 * Time: 9:54 PM
 */
namespace App\Services;


class WeatherService
{
    private $httpService;
    private $url;
    private $key;
    /**
     * WeatherService constructor.
     * @param HttpService $httpService
     */
    public function __construct(HttpService $httpService)
    {
        $this->url = env('WEATHER_API_URL');
        $this->key = env('WEATHER_API_KEY');
        $this->httpService = $httpService;
    }

    /**
     * @param $city
     * @return array
     */
    public function getRequestParams($city){
        return [
            'query' => ['key' => $this->key, 'q' => $city, 'format' => 'json']
        ];
    }

    /**
     * @param string $city
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWeatherByCity($city){
        if($city == null){
            throw new Exception();
        }
        return $this->httpService->request('GET', $this->url, $this->getRequestParams($city));
    }
}
