<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 6/22/19
 * Time: 4:45 PM
 */

namespace App\Services;


use App\Order;
use Exception;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * @return mixed
     */
    public function getOrders()
    {
        $orders = DB::table('order_products as op')
            ->join('products as pr', 'op.product_id', 'pr.id')
            ->join('orders', 'op.order_id', 'orders.id')
            ->join('partners', 'orders.partner_id', 'partners.id')
            ->groupBy('op.order_id')
            ->selectRaw(
                'op.order_id, 
                sum(op.price * op.quantity) as price,
                group_concat(pr.name) as products,
                partners.name as partner,
                orders.status as status
              ')
            ->get();

        return $orders;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOrderById($id)
    {
        $order = DB::table('order_products as op')
            ->join('products as pr', 'op.product_id', 'pr.id')
            ->join('orders', 'op.order_id', 'orders.id')
            ->join('partners', 'orders.partner_id', 'partners.id')
            ->groupBy('op.order_id')
            ->where('orders.id','=',$id)
            ->selectRaw(
                'op.order_id, 
                sum(op.price * op.quantity) as price,
                group_concat( CONCAT( pr.name, " количество: ", op.quantity) ) as products,
                partners.id as partner_id,
                partners.name as partner,
                orders.status as status,
                orders.client_email as email
              ')
            ->first();

        return json_encode($order);
    }

    /**
     * @param $params
     * @return mixed
     * @throws Exception
     */
    public function editOrder($params = null)
    {
        if (empty($params)){
            throw new Exception('параметры не найдены');
        }
        $order = Order::find($params['id']);
        $order->client_email = $params['email'];
        $order->partner_id = $params['partner_id'];
        $order->status = $params['status'];

        $order->save();
        return ['result' => true];
    }
}
