<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 6/21/19
 * Time: 10:22 PM
 */

namespace App\Services;


use GuzzleHttp\Client;

class HttpService
{
    /** @var Client $client */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $method
     * @param $url
     * @param $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $url, $params ){
        return $this->client->request($method, $url, $params)->getBody()->getContents();
    }
}
