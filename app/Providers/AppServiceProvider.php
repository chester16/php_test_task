<?php

namespace App\Providers;

use App\Services\HttpService;
use App\Services\OrderService;
use App\Services\PartnerService;
use App\Services\WeatherService;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HttpService::class, function($app){
            return new HttpService();
        });

        $this->app->singleton(WeatherService::class, function ($app){
            return new WeatherService(new HttpService());
        });

        $this->app->singleton(OrderService::class, function ($app){
            return new OrderService();
        });

        $this->app->singleton(PartnerService::class, function ($app){
            return new PartnerService();
        });
    }
}
