<?php

namespace App\Http\Controllers;

use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getAll(OrderService $orderService)
    {
        return $orderService->getOrders();
    }

    public function getById(OrderService $orderService, $id)
    {
        return $orderService->getOrderById($id);
    }

    public function edit(OrderService $orderService, Request $request)
    {
        return $orderService->editOrder($request->post('params'));
    }
}
