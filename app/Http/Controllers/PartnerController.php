<?php

namespace App\Http\Controllers;

use App\Services\PartnerService;

class PartnerController extends Controller
{
    /**
     * @param PartnerService $partnerService
     * @return mixed
     */
    public function all(PartnerService $partnerService)
    {
        return $partnerService->getAll();
    }
}
