<?php

namespace App\Http\Controllers;

use App\Services\WeatherService;;

class WeatherController extends Controller
{
    /**
     * @param WeatherService $weatherService
     * @param $city
     * @return string
     * Client $client
     */
    public function current(WeatherService $weatherService, $city){
        return $weatherService->getWeatherByCity($city);
    }
}
